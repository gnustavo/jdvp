function disableInlineEditForCustomField( customfieldId )
{

    if (typeof JIRA.Issues === 'undefined')
    {
        return;
    }

    var _this = this;
    JIRA.bind( JIRA.Events.NEW_CONTENT_ADDED, function ( a, b )
    {
        _this._disableInlineEditForCustomField( customfieldId );

    } );
    this._disableInlineEditForCustomField( customfieldId );
}

function _disableInlineEditForCustomField( customfieldId )
{
    var view = AJS.$( JIRA.Issues.IssueFieldUtil.getFieldSelector( customfieldId ) );
    var disableFunction = function ( e )
    {
        if (AJS.$( this ).is( '.editable-field' ) || (AJS.$( this ).is( '.editable-field > *' )))
        {
            console.log( 'disabling inline edit for ' + customfieldId );
            view.removeClass( 'editable-field' );
            view.removeClass( 'inactive' );
            view.attr( 'title', AJS.I18n.getText( "at.celix.jira.plugins.zones.fieldDisabled" ) );
            view.find( '.icon' ).remove();
            view.unbind( 'mouseenter' ).unbind( 'mouseleave' ).unbind( 'mousemove' ).unbind( 'mouseover' );
        }
    };
    if (view.is( ':hover' )){
        view.delegate( '*,', 'mouseover', disableFunction );
        view.hover( disableFunction );
        view.mousemove( disableFunction );
    }
    else
    {
        view.hover( disableFunction );
    }
}
